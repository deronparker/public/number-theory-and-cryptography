pub mod over_fp;

use num::BigInt;

use crate::i;

#[derive(PartialEq, Debug, Clone)]
/// coordinate
pub enum Coord {
    Integer(BigInt),
    Infinity,
}
impl Coord {
    pub fn is_infinity(&self) -> bool {
        self == &Self::Infinity
    }
    pub fn to_i64(&self) -> Option<BigInt> {
        if let Self::Integer(i) = self {
            Some(i.clone())
        } else {
            None
        }
    }
}
#[derive(PartialEq, Debug, Clone)]
pub struct Point {
    pub x: Coord,
    pub y: Coord,
}
impl Point {
    pub fn from_i128s(x: i128, y: i128) -> Self {
        Self {
            x: Coord::Integer(i!(x)),
            y: Coord::Integer(i!(y)),
        }
    }
    pub fn from_bigints(x: BigInt, y: BigInt) -> Self {
        Self {
            x: Coord::Integer(x),
            y: Coord::Integer(y),
        }
    }
    pub fn to_bigints(&self) -> (BigInt, BigInt) {
        if let (Coord::Integer(x), Coord::Integer(y)) = (&self.x, &self.y) {
            (x.clone(), y.clone())
        } else {
            panic!("x or y is not an integer: ({:?}, {:?})", self.x, self.y)
        }
    }
    pub fn infinity() -> Self {
        Self {
            x: Coord::Infinity,
            y: Coord::Infinity,
        }
    }
    /// checks whether both coordinates are infinity
    pub fn is_infinity(&self) -> bool {
        self.x.is_infinity() && self.y.is_infinity()
    }
}
