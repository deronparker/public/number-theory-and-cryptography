// TODO: replace custom power_mod with modpow
use num::{traits::Euclid, BigInt, Integer, Zero};
use std::fmt::Display;

use crate::{gcd::xgcd, macros::i};

#[derive(Debug, PartialEq)]
/// error while finding the inverse of `A mod M`
// TODO: rename to InverseError
pub enum InverseModError {
    DoesNotExist,
}
// impl Display for InverseModError {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         match self {
//             InverseModError::DoesNotExist { A, M } => {
//                 write!(f, "{}^-1 mod {} does not exist", A, M)
//             }
//         }
//     }
// }

/// determine the modular inverse of A modulo M
#[allow(non_snake_case)]
// TODO: rename to inverse
pub fn inverse_mod(A: &BigInt, M: &BigInt) -> Result<BigInt, InverseModError> {
    let a = A.rem_euclid(M);
    let res = xgcd(&a, M, false);
    let x = res.coefs.x;
    // let y = res.coefs.y;
    let gcd = res.gcd;

    if gcd == i!(1) {
        Ok((x.rem_euclid(M) + M).rem_euclid(M))
    } else {
        Err(InverseModError::DoesNotExist)
    }
}

/// determine whether `c` is a square for an odd prime `p` where `c != 0 mod p`
/// assumes `p` is prime
pub fn is_square(c: &BigInt, p: &BigInt) -> bool {
    if p.is_even() {
        panic!("p is not prime")
    } else if c.rem_euclid(p) == BigInt::zero() {
        panic!("c = 0 mod p")
    }
    c.modpow(&((p - 1) / BigInt::from(2)), p) == BigInt::from(1)
}

#[derive(PartialEq, Debug)]
pub enum SqrtModPError {
    DoesNotExist { c: BigInt, p: BigInt },
    Peq1mod8 { p: BigInt },
}
impl Display for SqrtModPError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SqrtModPError::DoesNotExist { c, p } => {
                write!(f, "the square root of {} mod {} does not exist", c, p)
            }
            SqrtModPError::Peq1mod8 { p } => write!(f, "p = {} = 1 mod 8", p),
        }
    }
}

/// solves x^2 = c (mod p) for x
/// note that the negation of the returned value is also a solution
pub fn sqrt_mod_p(c: &BigInt, p: &BigInt) -> Result<BigInt, SqrtModPError> {
    if !is_square(c, p) {
        Err(SqrtModPError::DoesNotExist {
            c: c.clone(),
            p: p.clone(),
        })
    } else if p.rem_euclid(&i!(4)) == i!(3) {
        Ok(c.modpow(&((p + 1) / 4), p))
    } else if p.rem_euclid(&i!(8)) == i!(5) {
        if c.modpow(&((p - i!(1)) / i!(4)), p) == i!(1) {
            Ok(c.modpow(&((p + 3) / 8), p))
        } else {
            Ok(
                (i!(2).modpow(&((p - i!(1)) / i!(4)), p) * c.modpow(&((p + i!(3)) / i!(8)), p))
                    .rem_euclid(p),
            )
        }
    } else {
        Err(SqrtModPError::Peq1mod8 { p: p.clone() })
    }
}

#[cfg(test)]
mod tests {
    use num::BigInt;

    use crate::{
        gcd::xgcd,
        macros::i,
        modular::{inverse_mod, InverseModError},
    };

    #[test]
    fn xgcd01() {
        assert_eq!(xgcd(&i!(1432), &i!(123211), false).gcd, i!(1))
    }
    #[test]
    fn inverse_mod01() {
        assert_eq!(inverse_mod(&i!(3), &i!(11)).unwrap(), i!(4))
    }
    #[test]
    fn inverse_mod_fail01() {
        assert_eq!(
            inverse_mod(&i!(251), &i!(387293)).err().unwrap(),
            InverseModError::DoesNotExist
        )
    }
    #[test]
    fn power_mod01() {
        assert_eq!(i!(2).modpow(&i!(8), &i!(500)), i!(256))
    }
    #[test]
    fn power_mod02() {
        assert_eq!(i!(48).modpow(&i!(293), &i!(3892029)), i!(992844))
    }

    mod sqrt_mod_p {
        use num::BigInt;

        use crate::{
            macros::i,
            modular::{sqrt_mod_p, SqrtModPError},
        };

        #[test]
        fn n01() {
            assert_eq!(i!(1448), {
                match sqrt_mod_p(&i!(38), &i!(7331)) {
                    Ok(rt) => rt,
                    Err(e) => panic!("{}", e),
                }
            })
        }
        #[test]
        fn n02() {
            assert_eq!(
                SqrtModPError::DoesNotExist {
                    c: i!(38),
                    p: i!(8675309)
                },
                sqrt_mod_p(&i!(38), &i!(8675309)).unwrap_err()
            )
        }
        #[test]
        fn n03() {
            assert_eq!(i!(1638340), sqrt_mod_p(&i!(382), &i!(8675309)).unwrap())
        }
    }
}
