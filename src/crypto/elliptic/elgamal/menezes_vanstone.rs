use num::{traits::Euclid, BigInt};

use crate::{
    elliptic::{over_fp::Curve, Coord, Point},
    i,
    modular::{self, inverse_mod, sqrt_mod_p, InverseModError, SqrtModPError},
};

#[derive(Debug, PartialEq)]
pub struct Plaintext {
    pub m1: BigInt,
    pub m2: BigInt,
}

#[derive(Debug, PartialEq)]
pub struct Ciphertext {
    pub R: Point,
    pub c1: BigInt,
    pub c2: BigInt,
}

/// key Q_A constructed from n_A*P
#[derive(Debug, PartialEq)]
pub struct PubKey {
    /// the public key
    pub Q_A: Point,
    /// published (shared) point
    pub P: Point,
    /// published curve
    pub curve: Curve,
    /// secret multiplier, if known
    pub n_A: Option<BigInt>,
}
impl PubKey {
    /// create new key using secret
    pub fn from_secret(n_A: BigInt, E: Curve, P: Point) -> Self {
        Self {
            Q_A: E.double_and_add(&n_A, P.clone()),
            P,
            curve: E,
            n_A: Some(n_A),
        }
    }
    /// encrypt "plaintexts" `m1` and `m2`
    /// * `k` - random number
    // TODO: use &plain
    pub fn encrypt(&self, plain: Plaintext, k: &BigInt) -> Ciphertext {
        let R = self.curve.double_and_add(k, self.P.clone());
        let S = self.curve.double_and_add(k, self.Q_A.clone());
        if let (Coord::Integer(xS), Coord::Integer(yS)) = (&S.x, &S.y) {
            let c1 = (xS * plain.m1).rem_euclid(&self.curve.p);
            let c2 = (yS * plain.m2).rem_euclid(&self.curve.p);
            Ciphertext { R, c1, c2 }
        } else {
            panic!("S contains infinite coord {:?}", S);
        }
    }
    // TODO: use &cipher
    pub fn decrypt(&self, cipher: Ciphertext) -> Plaintext {
        let n_A = self.n_A.clone().expect("secret multiplier n_A unknown");
        let T = self.curve.double_and_add(&n_A, cipher.R);
        let (xT, yT);
        if let (Coord::Integer(_xT), Coord::Integer(_yT)) = (&T.x, &T.y) {
            xT = _xT;
            yT = _yT;
        } else {
            panic!("T contains infinite coord {:?}", T);
        }
        let xT_inv = inverse_mod(xT, &self.curve.p).expect("failed to determine inverse of xT");
        let yT_inv = inverse_mod(yT, &self.curve.p).expect("failed to determine inverse of yT");
        let m1 = (xT_inv * cipher.c1).rem_euclid(&self.curve.p);
        let m2 = (yT_inv * cipher.c2).rem_euclid(&self.curve.p);
        Plaintext { m1, m2 }
    }
    /// decrypt [cipher](Ciphertext) only knowing the m1 portion of the [plaintext](Plaintext)
    pub fn decrypt_with_m1(
        &self,
        cipher: &Ciphertext,
        m1: BigInt,
    ) -> Result<Plaintext, DecryptWithM1Error> {
        use DecryptWithM1Error as FnError;
        let p = &self.curve.p;
        let m1_inv = match inverse_mod(&m1, p) {
            Ok(m) => Ok(m),
            Err(e) => Err(FnError::InverseM1Error(e)),
        }?;

        let xS = (&cipher.c1 * m1_inv).rem_euclid(p);
        debug_assert_eq!(
            m1,
            (inverse_mod(&xS, p).unwrap() * &cipher.c1).rem_euclid(p)
        );
        let yS = match self.curve.y(&xS) {
            Ok(y) => Ok(y),
            Err(e) => Err(FnError::YsCalcError(e)),
        }?;
        let yS_inv = match inverse_mod(&yS, p) {
            Ok(y) => Ok(y),
            Err(e) => Err(FnError::InverseYsError(e)),
        }?;
        Ok(Plaintext {
            m1,
            m2: (yS_inv * &cipher.c2).rem_euclid(p),
        })
    }
    /// decrypt [cipher](Ciphertext) only knowing the m1 portion of the [plaintext](Plaintext)
    /// (version 2)
    pub fn decrypt_with_m1_v2(
        &self,
        cipher: &Ciphertext,
        m1: BigInt,
    ) -> Result<Plaintext, DecryptWithM1v2Error> {
        use DecryptWithM1v2Error as FnErr;
        let A = &self.curve.A;
        let B = &self.curve.B;
        let p = &self.curve.p;
        let c1 = &cipher.c1;
        let c2 = &cipher.c2;

        let g1 = c2.modpow(&i!(2), p) * m1.modpow(&i!(3), p);
        let g2 = match inverse_mod(
            &(c1.modpow(&i!(3), p) + A * c1 * m1.modpow(&i!(2), p) + B * m1.modpow(&i!(3), p)),
            p,
        ) {
            Ok(g) => Ok(g),
            Err(e) => Err(FnErr::InverseError(e)),
        }?;
        let sq = (g1 * g2).rem_euclid(p);
        let m2 = match sqrt_mod_p(&sq, p) {
            Ok(m) => Ok(m),
            Err(e) => Err(FnErr::M2CalcError(e)),
        }?;
        Ok(Plaintext { m1, m2 })
    }
}
#[derive(Debug)]
pub enum DecryptWithM1Error {
    /// error while calculating `m1^-1`
    InverseM1Error(InverseModError),
    /// error while calculating `y_S` at `x_S`
    YsCalcError(SqrtModPError),
    /// error while calculating `y_S^-1`
    InverseYsError(InverseModError),
}
#[derive(Debug)]
pub enum DecryptWithM1v2Error {
    /// error while calculating m2
    M2CalcError(SqrtModPError),
    /// error while calculating an inverse
    InverseError(InverseModError),
}

#[cfg(test)]
mod tests {
    use num::{traits::Euclid, BigInt};

    use crate::{
        crypto::elliptic::elgamal::menezes_vanstone::Plaintext,
        elliptic::{over_fp::Curve, Point},
        i,
    };

    use super::{Ciphertext, PubKey};

    #[test]
    fn genkey() {
        let E = Curve {
            A: i!(13),
            B: i!(37),
            p: i!(7331),
        };
        let P = Point::from_i128s(101, 6124);
        let n_A = i!(111);
        let result = PubKey::from_secret(n_A.clone(), E.clone(), P.clone());
        let expected = PubKey {
            Q_A: Point::from_i128s(6271, 6224),
            P,
            curve: E,
            n_A: Some(n_A),
        };
        assert_eq!(expected, result);
    }
    #[test]
    fn encrypt() {
        let E = Curve {
            A: i!(13),
            B: i!(37),
            p: i!(7331),
        };
        let P = Point::from_i128s(101, 6124);
        let n_A = i!(111);
        let key = PubKey {
            Q_A: Point::from_i128s(6271, 6224),
            P,
            curve: E,
            n_A: Some(n_A),
        };
        let m1 = i!(12);
        let m2 = i!(21);
        let k = i!(121);
        let result = key.encrypt(
            Plaintext {
                m1: m1.clone(),
                m2: m2.clone(),
            },
            &k,
        );
        let expected = Ciphertext {
            R: Point::from_i128s(1980, 1861),
            c1: i!(2707),
            c2: i!(2778),
        };
        assert_eq!(expected, result);
    }
    #[test]
    fn decrypt() {
        let E = Curve {
            A: i!(13),
            B: i!(37),
            p: i!(7331),
        };
        let P = Point::from_i128s(101, 6124);
        let n_A = i!(111);
        let key = PubKey {
            Q_A: Point::from_i128s(6271, 6224),
            P,
            curve: E,
            n_A: Some(n_A),
        };
        let cipher = Ciphertext {
            R: Point::from_i128s(1980, 1861),
            c1: i!(2707),
            c2: i!(2778),
        };
        let result = key.decrypt(cipher);
        let expected = Plaintext {
            m1: i!(12),
            m2: i!(21),
        };
        assert_eq!(expected, result);
    }
    #[test]
    fn decrypt_with_m1() {
        let key = PubKey {
            Q_A: Point::from_i128s(6271, 6224),
            P: Point::from_i128s(101, 6124),
            curve: Curve {
                A: i!(13),
                B: i!(37),
                p: i!(7331),
            },
            n_A: None,
        };
        let cipher = Ciphertext {
            R: Point::from_i128s(1980, 1861),
            c1: i!(2707),
            c2: i!(2778),
        };
        let expected = Plaintext {
            m1: i!(12),
            m2: i!(21),
        };
        let result = key.decrypt_with_m1(&cipher, i!(12)).unwrap();
        assert_eq!(expected, result);
    }
    #[test]
    fn decrypt_with_m1v2() {
        let key = PubKey {
            Q_A: Point::from_i128s(6271, 6224),
            P: Point::from_i128s(101, 6124),
            curve: Curve {
                A: i!(13),
                B: i!(37),
                p: i!(7331),
            },
            n_A: None,
        };
        let p = &key.curve.p;
        let cipher = Ciphertext {
            R: Point::from_i128s(1980, 1861),
            c1: i!(2707),
            c2: i!(2778),
        };
        let expected = Plaintext {
            m1: i!(12),
            m2: i!(21),
        };
        let mut result = key.decrypt_with_m1_v2(&cipher, i!(12)).unwrap();
        result.m2 = (-&result.m2).rem_euclid(p);
        assert!(
            expected == result
                || expected
                    == Plaintext {
                        m1: result.m2.clone(),
                        m2: (-result.m2).rem_euclid(p)
                    }
        );
    }
}
