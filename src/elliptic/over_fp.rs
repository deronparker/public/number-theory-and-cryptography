use num::{range, traits::Euclid, BigInt, Integer, Signed, Zero};

use crate::{
    elliptic::Coord,
    i,
    modular::{inverse_mod, sqrt_mod_p, SqrtModPError},
};

use super::Point;

/// elliptic curve of the form `y^2 = x^3 + Ax + B` over `Fp`
#[derive(Debug, PartialEq, Clone)]
pub struct Curve {
    pub A: BigInt,
    pub B: BigInt,
    pub p: BigInt,
}

impl Curve {
    /// determine y at x, if it exists
    pub fn y(&self, x: &BigInt) -> Result<BigInt, SqrtModPError> {
        let A = &self.A;
        let B = &self.B;
        let p = &self.p;
        let sq = x.modpow(&i!(3), p) + A * x + B;
        sqrt_mod_p(&sq, p)
    }
    /// add two [points](Point) `P1` and `P2` on [self](Self) over Fp
    pub fn add(&self, P1: &Point, P2: &Point) -> Point {
        if P1.is_infinity() {
            return P2.clone();
        } else if P2.is_infinity() {
            return P1.clone();
        }
        if let (Coord::Integer(x1), Coord::Integer(y1), Coord::Integer(x2), Coord::Integer(y2)) =
            (P1.x.clone(), P1.y.clone(), P2.x.clone(), P2.y.clone())
        {
            let ll;
            if x1 == x2 && y1.rem_euclid(&self.p) == (-&y2).rem_euclid(&self.p) {
                return Point::infinity();
            } else if x1 == x2 && y1 == y2 {
                ll = ((i!(3) * x1.modpow(&i!(2), &self.p) + &self.A)
                    * inverse_mod(&(i!(2) * &y1), &self.p).unwrap())
                .rem_euclid(&self.p);
            } else {
                ll = ((y2 - &y1) * inverse_mod(&(&x2 - &x1), &self.p).unwrap()).rem_euclid(&self.p);
            }
            let x3 = (ll.modpow(&i!(2), &self.p) - &x1 - x2).rem_euclid(&self.p);
            let y3 = (ll * (x1 - &x3) - y1).rem_euclid(&self.p);
            return Point {
                x: Coord::Integer(x3),
                y: Coord::Integer(y3),
            };
        }
        panic!("was given a point with one coordinate at infinity");
    }
    /// calculate nP on self
    pub fn double_and_add(&self, n: &BigInt, P: Point) -> Point {
        let mut n = n.clone();
        let mut Q = P;
        let mut R = Point::infinity();
        let mut make_neg = false;
        if n < BigInt::zero() {
            n = n.abs();
            make_neg = true;
        }
        while n > BigInt::zero() {
            if n.is_odd() {
                R = self.add(&R, &Q);
            }
            Q = self.add(&Q, &Q);
            n = n / BigInt::from(2);
        }
        if make_neg {
            let x = R.x.to_i64().unwrap();
            let y = (-R.y.to_i64().unwrap()).rem_euclid(&self.p);
            Point::from_bigints(x, y)
        } else {
            R
        }
    }
    /// find `n` such that `Q = nP`
    /// * `start` - minimum (inclusive) for `n`
    /// * `stop` - maximum (exclusive) for `n`
    pub fn solve_DLP_brute_force(
        &self,
        P: &Point,
        Q: &Point,
        start: BigInt,
        stop: BigInt,
    ) -> Option<BigInt> {
        let mut nP = P.clone();
        for n in range(start, stop) {
            nP = self.add(&nP, &P);
            if &nP == Q {
                return Some(n);
            }
        }
        None
    }
}
#[cfg(test)]
mod tests {
    use num::BigInt;

    use crate::macros::i;

    use super::*;

    mod y {
        use std::str::FromStr;

        use crate::is;

        use super::super::*;
        #[test]
        fn _01() {
            let curve = Curve {
                A: i!(13),
                B: i!(37),
                p: i!(7331),
            };
            let x = i!(101);
            let y_expected = i!(6124);
            let y_result = curve.y(&x).unwrap();
            assert_eq!(y_expected, y_result);
        }
        #[test]
        fn _02() {
            let curve = Curve {
                A: i!(-3),
                B: is!(
                    "41058363725152142129326129780047268409114441015993725554835256314039467401291"
                ),
                p: is!("115792089210356248762697446949407573530086143415290314195533631308867097853951"),
            };
            let x = is!(
                "48439561293906451759052585252797914202762949526041747995844080717082404635286"
            );
            let y_expected = is!(
                "36134250956749795798585127919587881956611106672985015071877198253568414405109"
            );
            let y_result = curve.y(&x).unwrap();
            assert_eq!(y_expected, y_result);
        }
    }

    #[test]
    fn add_over_Fp01() {
        let E = Curve {
            A: i!(1),
            B: i!(-6),
            p: i!(11),
        };
        let P = Point::from_i128s(2, 5);
        let Q = Point::from_i128s(5, 8);
        let expected = Point::from_i128s(5, 3);
        assert_eq!(expected, E.add(&P, &Q))
    }

    #[test]
    fn double_and_add01() {
        let N = i!(124627);
        let A = i!(42);
        let B = i!(123318);
        let E = Curve { A, B, p: N };
        let n = i!(24);
        let P = Point::from_i128s(11, 22);
        let result = E.double_and_add(&n, P);
        let expected = Point::from_i128s(123761, 40735);

        assert_eq!(expected, result);
    }
    #[test]
    fn double_and_add02() {
        let N = i!(133333337);
        let A = i!(38);
        let B = i!(133333262);
        let E = Curve { A, B, p: N };
        let n = i!(362879);
        let P = Point::from_i128s(2, 3);
        let result = E.double_and_add(&n, P);
        let expected = Point::from_i128s(9139443, 116403646);

        assert_eq!(expected, result);
    }
    mod solve_DLP_brute_force {
        use std::str::FromStr;

        use num::BigInt;

        use crate::macros::{i, is};

        use super::*;

        #[test]
        #[ignore]
        fn n01() {
            let A = i!(-3);
            let B = is!(
                "41058363725152142129326129780047268409114441015993725554835256314039467401291"
            );
            let p = is!(
                "115792089210356248762697446949407573530086143415290314195533631308867097853951"
            );
            let P = Point::from_bigints(
                is!(
                    "48439561293906451759052585252797914202762949526041747995844080717082404635286"
                ),
                is!(
                    "36134250956749795798585127919587881956611106672985015071877198253568414405109"
                ),
            );
            let Q = Point::from_bigints(
                is!(
                    "32255260455659832143532082006225685676684606814365121409949287251756083287375"
                ),
                is!(
                    "44659010038499630122514923844493677359725474228155072218332820659646489832109"
                ),
            );
            let nBound = i!(10).pow(5);
            let E = Curve { A, B, p };
            assert_eq!(
                i!(13337),
                E.solve_DLP_brute_force(&P, &Q, i!(2), nBound).unwrap()
            )
        }
    }
}
