use num::BigInt;

use crate::macros::i;

/// use the Euclidean Algorithm to determine the gcd of two numbers
/// * `output` - print to stdout
pub fn euclidean_gcd(a: &BigInt, b: &BigInt, output: bool) -> BigInt {
    // https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/the-euclidean-algorithm
    let mut A = a.clone();
    let mut B = b.clone();
    loop {
        if A == i!(0) {
            println!("gcd({}, {}) = {}", a, b, B);
            return B;
        } else if B == i!(0) {
            println!("gcd({}, {}) = {}", a, b, A);
            return A;
        }
        // remainder
        let R = &A % &B;

        if output {
            // coefficient
            let Q = &A / &B;
            println!("({}, {}) | A = B({}) + {}", A, B, Q, R)
        }

        A = B;
        B = R;
    }
}

/// `x` and `y` in `ax + by = gcd(a,b)`
#[derive(Debug)]
pub struct BezoutCoefficients {
    /// `x` in `ax`
    pub x: BigInt,
    /// `y` in `by`
    pub y: BigInt,
}

#[allow(non_camel_case_types)]
#[derive(Debug)]
pub struct XgcdResult {
    pub coefs: BezoutCoefficients,
    pub gcd: BigInt,
}
impl XgcdResult {
    pub fn new(x: BigInt, y: BigInt, d: BigInt) -> Self {
        Self {
            coefs: BezoutCoefficients { x, y },
            gcd: d,
        }
    }
}

/// determine the integers x and y (Bezout coefficients) such that `ax + by = gcd(a,b)`
/// * `output` - print to stdout
#[allow(non_snake_case)]
pub fn xgcd(a: &BigInt, b: &BigInt, output: bool) -> XgcdResult {
    // https://brilliant.org/wiki/extended-euclidean-algorithm/
    let mut A = a.clone();
    let mut B = b.clone();

    let mut x = i!(0);
    let mut y = i!(1);
    let mut u = i!(1);
    let mut v = i!(0);

    let mut q;
    let mut r;
    let mut m;
    let mut n;

    while A != i!(0) {
        q = &B / &A;
        r = &B % &A;
        m = x - &u * &q;
        n = y - &v * &q;
        B = A;
        A = r;
        x = u;
        y = v;
        u = m;
        v = n;
    }
    let gcd = B;
    if output {
        todo!("output currently unusable")
    }
    XgcdResult::new(x, y, gcd)
}
