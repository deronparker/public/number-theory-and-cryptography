/// whether a divides b
pub fn divides(a: i64, b: i64) -> bool {
    (b / a) * a == b
}
