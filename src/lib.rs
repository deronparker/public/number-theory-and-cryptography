#![allow(non_snake_case)]

/// cryptography
pub mod crypto;
/// elliptic curve operations
pub mod elliptic;
/// greatest common denominator operations
pub mod gcd;
/// miscellaneous
pub mod misc;
/// modular arithmetic
pub mod modular;

pub mod macros {
    #[macro_export]
    macro_rules! i {
        ($int:expr) => {
            BigInt::from($int)
        };
    }
    #[macro_export]
    macro_rules! is {
        ($str:expr) => {
            BigInt::from_str($str).unwrap()
        };
    }
    pub(crate) use i;
    pub(crate) use is;
}

pub use num;
